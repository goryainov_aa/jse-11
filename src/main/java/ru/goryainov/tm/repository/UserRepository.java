package ru.goryainov.tm.repository;

import ru.goryainov.tm.entity.User;
import ru.goryainov.tm.enumerated.Role;
import ru.goryainov.tm.util.HashMD5;

import java.util.ArrayList;
import java.util.List;

public class UserRepository {

    private List<User> users = new ArrayList<>();

    public User create(final String firstName, final String lastName, final String middleName,
                       final String login, final String password, final Role role) {
        final User user = new User(firstName, lastName, middleName, login, HashMD5.md5(password), role);
        users.add(user);
        return user;
    }

    public User create(final String firstName, final String lastName, final String middleName,
                       final String login, final String password) {
        final User user = new User(firstName, lastName, middleName, login, HashMD5.md5(password));
        user.setRole(Role.USER);
        users.add(user);
        return user;
    }

    public User create(final String login, final String password) {
        return create("", "", "", login, password, Role.USER);
    }

    public void clear() {
        users.clear();
    }

    public int size() {
        return users.size();
    }


    public List<User> findAll() {
        return users;
    }

    public User findByIndex(final int index) {
        return users.get(index);
    }

    public User findById(final Long id) {
        for (User user : users) {
            if (user.getId().equals(id)) {
                return user;
            }
        }
        return null;
    }

    public User findByLogin(final String login) {
        for (User user : users) {
            if (user.getLogin().equals(login)) {
                return user;
            }
        }
        return null;
    }

    public User removeByIndex(final int index) {
        User user = findByIndex(index);
        users.remove(user);
        return user;
    }

    public User removeById(final Long id) {
        User user = findById(id);
        users.remove(user);
        return user;
    }

    public User removeByLogin(final String login) {
        for (User user : users) {
            if (user.getLogin().equals(login)) {
                users.remove(user);
                return user;
            }
        }
        return null;
    }

    public User removeByName(final String firstName, final String lastdName, final String middleName) {
        for (User user : users) {
            if (user.getFirstName().equals(firstName) &&
                    user.getLastName().equals(lastdName) &&
                    user.getMiddleName().equals(middleName)) {
                users.remove(user);
                return user;
            }
        }
        return null;
    }


    public User updateById(final Long id, final String firstName, final String lastName, final String middleName, final String login, final String password, final Role role) {
        for (User user : users) {
            if (user.getId().equals(id)) {
                user.setFirstName(firstName);
                user.setLastName(lastName);
                user.setMiddleName(middleName);
                user.setLogin(login);
                user.setPasswordHash(password);
                user.setRole(role);
                return user;
            }
        }
        return null;
    }


}
