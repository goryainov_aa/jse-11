package ru.goryainov.tm.controller;

import ru.goryainov.tm.entity.Project;
import ru.goryainov.tm.entity.Task;
import ru.goryainov.tm.entity.User;
import ru.goryainov.tm.service.UserService;

public class UserController extends AbstractController{

    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    /**
     * Cоздание пользователя
     */
    public int createUser() {
        System.out.println("[Create user]");
        System.out.println("[Please, enter user firstname:]");
        final String firstName = scanner.nextLine();
        System.out.println("[Please, enter user lastname:]");
        final String lastName = scanner.nextLine();
        System.out.println("[Please, enter user middlename:]");
        final String middleName = scanner.nextLine();
        System.out.println("[Please, enter user login:]");
        final String login = scanner.nextLine();
        System.out.println("[Please, enter user password:]");
        final String password = scanner.nextLine();
        System.out.println("Please, enter user role:");
        String role =scanner.nextLine();
        userService.create(firstName, lastName, middleName, login, password, role);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Изменение пользователя по индексу
     */
    public int updateUserByIndex() {
        System.out.println("[Update user]");
        System.out.println("[Please, enter user index:]");
        final int index = Integer.parseInt(scanner.nextLine()) - 1;
        final User user = userService.findByIndex(index);
        if (user == null) {
            System.out.println("[Fail]");
            return 0;
        }
        System.out.println("[Please, enter user firstname:]");
        final String firstName = scanner.nextLine();
        System.out.println("[Please, enter user lastname:]");
        final String lastName = scanner.nextLine();
        System.out.println("[Please, enter user middlename:]");
        final String middleName = scanner.nextLine();
        System.out.println("[Please, enter user login:]");
        final String login = scanner.nextLine();
        System.out.println("[Please, enter user password:]");
        final String password = scanner.nextLine();
        System.out.println("[Please, enter user role:]");
        final String role = scanner.nextLine();
        userService.update (user.getId(), firstName, lastName, middleName, login, password, role);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Изменение пользователя по идентификатору
     */
    public int updateUserById() {
        System.out.println("[Update user]");
        System.out.println("[Please, enter user id:]");
        final long id = Long.parseLong(scanner.nextLine());
        final User user = userService.findById(id);
        if (user == null) {
            System.out.println("[Fail]");
            return 0;
        }
        System.out.println("[Please, enter user firstname:]");
        final String firstName = scanner.nextLine();
        System.out.println("[Please, enter user lastname:]");
        final String lastName = scanner.nextLine();
        System.out.println("[Please, enter user middlename:]");
        final String middleName = scanner.nextLine();
        System.out.println("[Please, enter user login:]");
        final String login = scanner.nextLine();
        System.out.println("[Please, enter user password:]");
        final String password = scanner.nextLine();
        System.out.println("[Please, enter user role:]");
        final String role = scanner.nextLine();
        userService.update(user.getId(), firstName, lastName, middleName, login, password, role);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Изменение пользователя по логину
     */
    public int updateUserByLogin() {
        System.out.println("[Update user]");
        System.out.println("[Please, enter user login:]");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        if (user == null) {
            System.out.println("[Fail]");
            return 0;
        }
        System.out.println("[Please, enter user firstname:]");
        final String firstName = scanner.nextLine();
        System.out.println("[Please, enter user lastname:]");
        final String lastName = scanner.nextLine();
        System.out.println("[Please, enter user middlename:]");
        final String middleName = scanner.nextLine();
        System.out.println("[Please, enter user password:]");
        final String password = scanner.nextLine();
        System.out.println("[Please, enter user role:]");
        final String role = scanner.nextLine();
        userService.update(user.getId(), firstName, lastName, middleName, login, password, role);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Удаление пользователя из списка по имени
     */
    public int removeUserByName() {
        System.out.println("[Remove user by name]");
        System.out.println("[Please, enter user firstname:]");
        final String firstName = scanner.nextLine();
        System.out.println("[Please, enter user lastname:]");
        final String lastName = scanner.nextLine();
        System.out.println("[Please, enter user middlename:]");
        final String middleName = scanner.nextLine();
        final User user = userService.removeByName(firstName, lastName, middleName);
        if (user == null) System.out.println("[Fail]");
        else System.out.println("[Ok]");
        return 0;
    }

    /**
     * Удаление пользователя из списка по идентификатору
     */
    public int removeUserById() {
        System.out.println("[Remove user by id]");
        System.out.println("[Please, enter user id:]");
        final long id = scanner.nextLong();
        final User user = userService.removeById(id);
        if (user == null) System.out.println("[Fail]");
        else System.out.println("[Ok]");
        return 0;
    }

    /**
     * Удаление пользователя из списка по логину
     */
    public int removeUserByLogin() {
        System.out.println("[Remove user by login]");
        System.out.println("[Please, enter user login:]");
        final String login = scanner.nextLine();
        final User user = userService.removeByLogin(login);
        if (user == null) System.out.println("[Fail]");
        else System.out.println("[Ok]");
        return 0;
    }

    /**
     * Удаление пользователя из списка по индексу
     */
    public int removeUserByIndex() {
        System.out.println("[Remove user by index]");
        System.out.println("[Please, enter user index:]");
        final int index = scanner.nextInt() - 1;
        final User user = userService.removeByIndex(index);
        if (user == null) System.out.println("[Fail]");
        else System.out.println("[Ok]");
        return 0;
    }

    /**
     * Очистки проекта
     */
    public int clearUser() {
        System.out.println("[Clear user]");
        userService.clear();
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Просмотр списка пользователей
     */
    public void viewUser(final User user) {
        if (user == null) return;
        System.out.println("[View user]");
        System.out.println("Id: " + user.getId());
        System.out.println("FirstName: " + user.getFirstName());
        System.out.println("LasttName: " + user.getLastName());
        System.out.println("MiddleName: " + user.getMiddleName());
        System.out.println("Login: " + user.getLogin());
        System.out.println("Role: " + user.getRole());
        System.out.println("[Ok]");
    }

    /**
     * Просмотр пользователя по индексу
     */
    public int viewUserByIndex() {
        System.out.println("Enter, user index: ");
        final int index = scanner.nextInt() - 1;
        final User user = userService.findByIndex(index);
        viewUser(user);
        return 0;
    }

    /**
     * Просмотр пользователя по идентификатору
     */
    public int viewUserById() {
        System.out.println("Enter, user id: ");
        final Long id = scanner.nextLong();
        final User user = userService.findById(id);
        viewUser(user);
        return 0;
    }

    /**
     * Просмотр пользователя по логину
     */
    public int viewUserByLogin() {
        System.out.println("Enter, user login: ");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        viewUser(user);
        return 0;
    }

    /**
     * Вывод списка пользователей
     */
    public int listUser() {
        System.out.println("[List user]");
        int index = 1;
        for (final User user : userService.findAll()) {
            System.out.println(Integer.toString(index)  + '.');
            viewUser(user);
            index++;
        }
        System.out.println("[Ok]");
        return 0;
    }

}
