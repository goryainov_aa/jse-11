package ru.goryainov.tm.controller;

public class SystemController {

    /**
     * Вывод сведений о разработчике
     */
    public int displayAbout() {
        System.out.println("Andrey Goryainov");
        System.out.println("goryainov_aa@nlmk.ru");
        return 0;
    }

    /**
     * Вывод версии
     */
    public int displayVersion() {
        System.out.println("1.0.0");
        return 0;
    }

    /**
     * Вывод списка возможных параметров запуска
     */
    public int displayHelp() {
        System.out.println("version - Display application version.");
        System.out.println("about - Display developer info.");
        System.out.println("help - Display list of commands.");
        System.out.println("exit - Terminate console application.");
        System.out.println();
        System.out.println("project-list - display list of projects.");
        System.out.println("project-create - create new project by name.");
        System.out.println("project-clear - remove all projects.");
        System.out.println("project-view-by-index - display information about project by index.");
        System.out.println("project-view-by-id - display information about project by id.");
        System.out.println("project-remove-by-name - remove project by name.");
        System.out.println("project-remove-by-index - remove project by index.");
        System.out.println("project-remove-by-id - remove project by id.");
        System.out.println("project-update-by-index - update project by index.");
        System.out.println("project-update-by-id - update project by id.");
        System.out.println();
        System.out.println("task-list - display list of tasks.");
        System.out.println("task-create - create new task by name.");
        System.out.println("task-clear - remove all tasks.");
        System.out.println("task-view-by-index - display information about task by index.");
        System.out.println("task-view-by-id - display information about task by id.");
        System.out.println("task-remove-by-name - remove task by name.");
        System.out.println("task-remove-by-index - remove task by index.");
        System.out.println("task-remove-by-id - remove task by id.");
        System.out.println("task-update-by-index - update task by index.");
        System.out.println("task-update-by-id - update task by id.");
        System.out.println("task-list-by-project-id - display task list by project id.");
        System.out.println("task-add-to-project-by-ids - add task to project by ids.");
        System.out.println("task-remove-from-project-by-ids - remove task from project by ids.");
        System.out.println();
        System.out.println("user-list - display list of users.");
        System.out.println("user-create - create new user.");
        System.out.println("user-clear - remove all users.");
        System.out.println("user-update-by-id - update user by id.");
        System.out.println("user-update-by-index - update user by index.");
        System.out.println("user-update-by-login - update user by login.");
        System.out.println("user-remove-by-id - remove user by id.");
        System.out.println("user-remove-by-index - remove user by index.");
        System.out.println("user-remove-by-login - remove user by login.");
        System.out.println("user-remove-by-name - remove user by name.");
        System.out.println("user-view-by-id - display information about user by id.");
        System.out.println("user-view-by-index - display information about user by index.");
        System.out.println("user-view-by-login - display information about user by login.");
        return 0;
    }

    /**
     * Вывод приветствия
     */
    public void displayWelcome() {
        System.out.println("-= WELCOME TO TASK MANAGER =-");
    }

    /**
     * Вывод ошибки при вводе параметра запуска не из списка
     */
    public int displayError() {
        System.out.println("Error! Unknown program argument...");
        return -1;
    }

    public int displayError(Exception ex) {
        System.out.println("Error!" + ex.getMessage());
        return -1;
    }

    /**
     * Вывод завершения приложения
     */
    public int displayExit() {
        System.out.println("Terminate program...");
        return 0;
    }

}
